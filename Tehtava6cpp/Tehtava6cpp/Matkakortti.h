#pragma once
#include "stdafx.h"
const float HELSINKIHINTA = 4.0; //#define HELSINKIHINTA (float)4.0 
const float SEUTUHINTA = 5.5;
enum Matkatyyppi {HELSINKI, SEUTU}; // lueteltu 
using namespace std;

class Matkakortti
{
private:
	const string defaultOmistaja = "Korttia ei ole alustettu";
	string* kortinOmistaja;
	float* saldo;

public:
	Matkakortti();
	~Matkakortti();
	void alusta(string &omistaja);
	bool matkusta(Matkatyyppi tyyppi);
	string tulostaTiedot(); // pulma sovelluskerros (Matkakortti) tulee riippuvaiseksi kl:st�
	string& palautaNimi();
	float palautaSaldo();
	void lataa(float raha);
};

