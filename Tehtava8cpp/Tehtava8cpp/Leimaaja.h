#pragma once

#include "stdafx.h"

const int REKISTERIN_KOKO = 3;

using namespace std;

class Leimaustapahtuma;

class Leimaaja
{
private:

public:
	Leimaaja();
	~Leimaaja();
	bool leimaa(Matkakortti& kortti, Matkatyyppi tyyppi);
	string tulostaLeimaaja();
	void tulostaAikaleima(struct tm& aikaleima); // testilause
	void seuraava(Leimaustapahtuma** ptr);
	bool operator<<(Matkakortti& kortti);
	friend ostream& operator<<(ostream& os, Leimaaja& leimaaja);
};

