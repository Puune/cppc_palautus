// C-kielinen Levyluettelo 
//

#include "stdafx.h"
#define MAX 2

typedef struct kokoelma {
	char nimi[41];
	char tekija[31];
	int jvuosi;
} aanite;

aanite levy, * levyOsoitin, levyt[MAX];

using namespace std;

int kysyTiedot()
{
	/* pyyt�� ��nitteiden tiedot k�ytt�j�lt�. palauttaa arvonaan sy�tettyjen lukum��r�n*/
	char rivi[81];
	int i;
	//levyOsoitin=&levy; // asetetaan osoitin osoitamaan levy-muuttujaa
	printf("kirjoita ��nilevykokoelmasi tiedot. Lopetus: \"teoksen nimi\"-kent�ss� = *\n");
	i = 0;
	do
	{
		cout << "\nteoksen nimi? (40 merkki�) ";
		levyOsoitin = &levyt[i];
		cin >> levyOsoitin->nimi;

		if (levyOsoitin->nimi[0] != '*')
		{
			cout << "\ntekija? (30 merkkia) ";
			cin >> levyOsoitin->tekija;
			cout << "\njulkaisuvuosi? ";
			cin >> rivi;
			while (sscanf_s(rivi, "%d", &levyOsoitin->jvuosi) != 1)
			{
				cout << "\nantamasi tieto ei ollut numeerinen. Kirjoita uusi";
				cin >> rivi;
			}
			i++;
		}
	} while ((levyOsoitin->nimi[0] != '*') && (i < MAX));
	return(i);
}

void tulostaTiedot(int lkm)
/* tulostetaan levyt */
{
	int i = 0;
	system("cls");
	if (lkm == 0)
	{
		cout << "Et syottanyt yhtakaan levya" << endl;
	}
	else
	{
		for (i = 0; i < lkm; i++)
		{	// sarakkeet alkavat samasta tasosta
;			cout << levyt[i].nimi << string(30 - strlen(levyt[i].nimi), ' ')
				<< levyt[i].tekija << string(25 - strlen(levyt[i].tekija), ' ')
				<< levyt[i].jvuosi
				<< endl;
		}
	}
	cout << endl; // rivinvaihto
	_getch();
}

void main()
{
	tulostaTiedot(kysyTiedot());
}



