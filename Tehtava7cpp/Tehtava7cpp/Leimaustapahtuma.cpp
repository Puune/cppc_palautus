#include "Leimaustapahtuma.h"

Leimaustapahtuma::Leimaustapahtuma(Matkakortti& matkakortti, struct tm& aikaleima, enum Matkatyyppi& matkatyyppi)
{
	this->leimaaja = new string(matkakortti.palautaNimi());
	this->aikaleima = new struct tm (aikaleima);
	this->matkatyyppi = new enum Matkatyyppi(matkatyyppi);
	this->initialized = true;
}

Leimaustapahtuma::Leimaustapahtuma()
{
	this->leimaaja = new string("None");
	this->aikaleima = nullptr;
	this->matkatyyppi = nullptr;
}

Leimaustapahtuma::~Leimaustapahtuma()
{
	delete aikaleima;
	delete leimaaja;
	delete matkatyyppi;
}

string Leimaustapahtuma::lue()
{
	stringstream ss;
	ss << *leimaaja << " - ";
	ss << "Matkantyyppi: " << *matkatyyppi << " - ";
	ss << aikaleima->tm_hour << ":" << aikaleima->tm_min << ":" << aikaleima->tm_sec << endl;
	return ss.str();
}
