#pragma once
#include "stdafx.h"

using namespace std;

class Leimaustapahtuma
{
private:
	struct tm* aikaleima;
	enum Matkatyyppi* matkatyyppi;
	string* leimaaja;

public:
	bool initialized = false;
	Leimaustapahtuma(Matkakortti& matkakortti, struct tm& aikaleima, enum Matkatyyppi& matkatyyppi);
	Leimaustapahtuma();
	~Leimaustapahtuma();
	string lue();
};