#include <stdio.h>

void main()
{
	char sana1[21], sana2[21], yhdyssana[42];
	char* lukija;
	char* kirjoittaja = yhdyssana;

	printf("Kirjoita sana 1:");
	gets_s(sana1, 21);
	printf("Kirjoita sana 2:");
	gets_s(sana2, 21);
	
	lukija = sana1;
	while ((*kirjoittaja++ = *lukija++) != '\0');

	kirjoittaja--; //poistetaan '\n'

	lukija = sana2;
	while ((*kirjoittaja++ = *lukija++) != '\0');

	printf("%s", yhdyssana);
}