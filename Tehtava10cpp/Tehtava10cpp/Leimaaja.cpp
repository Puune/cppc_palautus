#include "stdafx.h"

using namespace std;

bool Leimaaja::leimaa(Matkakortti kortti, Matkatyyppi tyyppi)
{
    // kutsu matkakortin matkusta-operaatiota
    // jos saldo on riitt�v�
    //      kopioi matkakortin omistaja leimaajalle
    //      generoi aikaleima ja talleta se leimaajalle
    //      palauta true;
    // muuten
    //      palauta false;

    if (kortti.matkusta(tyyppi)) 
    {
        viimeisinLeimaaja = kortti.palautaNimi();
        time_t now = time(0);
        struct tm buf;
        localtime_s(&buf, &now);
        viimeisinAikaleima = buf;
        return true;
    }
    else 
    {
        return false;
    }
}

void Leimaaja::tulostaLeimaaja()
{
    if (viimeisinLeimaaja == "none")
    {
        cout << "Ei edellist� leimaa..";
        return;
    }
    cout << "Matkustaja: " << viimeisinLeimaaja << endl;
    tulostaAikaleima();
}

void Leimaaja::tulostaAikaleima()
{
    time_t      sekunnit;

    // Get time in seconds
    time(&sekunnit);

    // Convert time to struct tm form 
    localtime_s(&viimeisinAikaleima, &sekunnit);

    // Print local time as a string.
    std::cout << viimeisinAikaleima.tm_hour << ":" << viimeisinAikaleima.tm_min << ":" << viimeisinAikaleima.tm_sec; // C4996
    std::cout << "\n";
}
