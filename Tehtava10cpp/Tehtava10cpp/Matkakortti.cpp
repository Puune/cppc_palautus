#include "stdafx.h"

using namespace std;



Matkakortti::Matkakortti()
{
	kortinOmistaja = new string(defaultOmistaja);
	this->saldo = new float(0.0f);
}

Matkakortti::Matkakortti(const Matkakortti& kortti)
{	
	string temp = *kortti.kortinOmistaja;
	kortinOmistaja = &temp;
	*saldo = *kortti.saldo;
}

Matkakortti::~Matkakortti()
{
	delete saldo;
	delete kortinOmistaja;
}

void Matkakortti::alusta(string& omistaja)
{
	kortinOmistaja = &omistaja;
	this->saldo = new float(0.0f);
}

bool Matkakortti::matkusta(Matkatyyppi tyyppi)
{
	float hinta = 0;
	switch (tyyppi)
	{
	case HELSINKI:
		hinta = HELSINKIHINTA;
		break;

	case SEUTU:
		hinta = SEUTUHINTA;
		break;
	}

	float testi = *this->saldo - hinta;

	if (testi >= 0)
	{
		*this->saldo = testi;
		return true;
	}
	else
	{
		return false;
	}
}

string Matkakortti::tulostaTiedot()
{
	string out = "";
	out.append(*kortinOmistaja + ",");

	stringstream stream;
	stream << fixed << setprecision(2) << *saldo;
	out.append(stream.str());
	return out;
}

string& Matkakortti::palautaNimi()
{
	return *kortinOmistaja;
}

float Matkakortti::palautaSaldo()
{
	return (*this->saldo = 0.0f);
}

void Matkakortti::lataa(float raha)
{
	*this->saldo = *saldo + raha;
}
