#include <stdio.h>

void main()
{
	int values[10] = { 0 };
	int *ptr = values;

	for (int i = 0; i < 10; i++)
	{
		if(i%2 == 0)
		{
			printf("input %d:", i);
			scanf_s("%d", ptr);
		}
		ptr++;
	}

	ptr = values;
	for (size_t i = 0; i < 10; i++)
	{
		printf("At: %p %d -- value: %p %d \n", ptr, ptr, *ptr, *ptr);
		ptr++;
	}
}