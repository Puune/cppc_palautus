#include "Leimaaja.h"

Leimaustapahtuma tapahtumat[REKISTERIN_KOKO];
Leimaustapahtuma* operaattori;

using namespace std;

Leimaaja::Leimaaja()
{
    operaattori = tapahtumat;
}

Leimaaja::~Leimaaja()
{}

bool Leimaaja::leimaa(Matkakortti& kortti, Matkatyyppi tyyppi)
{
    // kutsu matkakortin matkusta-operaatiota
    // jos saldo on riitt�v�
    //      kopioi matkakortin omistaja leimaajalle
    //      generoi aikaleima ja talleta se leimaajalle
    //      palauta true;
    // muuten
    //      palauta false;

    if (kortti.matkusta(tyyppi)) 
    {
        time_t now = time(0);
        struct tm leima;
        localtime_s(&leima, &now);

        Leimaustapahtuma * tapahtuma = new Leimaustapahtuma(kortti, leima, tyyppi);
        
        *operaattori = *tapahtuma;
        
        seuraava(&operaattori);

        return true;
    }
    else 
    {
        return false;
    }
}

void Leimaaja::tulostaLeimaaja()
{
    Leimaustapahtuma* viimeisin = operaattori;

    do
    {
        if (operaattori->initialized)
        {
            cout << operaattori->lue();
        }
        seuraava(&operaattori);
    } while (viimeisin != operaattori);
}

void Leimaaja::tulostaAikaleima(struct tm& aikaleima)
{
    time_t      sekunnit;

    // Get time in seconds
    time(&sekunnit);

    // Convert time to struct tm form 
    localtime_s(&aikaleima, &sekunnit);

    // Print local time as a string.
    std::cout << aikaleima.tm_hour << ":" << aikaleima.tm_min << ":" << aikaleima.tm_sec; // C4996
    std::cout << "\n";
}

void Leimaaja::seuraava(Leimaustapahtuma** ptr)
{
    //Kun listan raja ylittyy, palaa alkuun
    if (*ptr == (Leimaustapahtuma*)(&tapahtumat + 1)-1) //Tuota listan viimeinen muistipaikka
    {
        *ptr = &tapahtumat[0];
    }
    else
    {
        *ptr = *ptr + 1;
    }
}