
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#define HELSINKIHINTA (float)2.50
#define SEUTUHINTA (float)3.80

enum Matkatyyppi { HELSINKI, SEUTU };

struct Matkakortti
{
	char omistaja[41];
	float saldo;
};


// prototyyppi vaaditaan, jos kutsu on ennen m��rittely�
void tulostaValikko();
void alustaKortti(struct Matkakortti* kortti);
void nollaaKortti(struct Matkakortti* kortti);
void lataaSaldo(struct Matkakortti* kortti);
int matkusta(struct Matkakortti* kortti, enum Matkatyyppi tyyppi);

int main(int argc, char* argv[])
{
	tulostaValikko();
	return 0;
}

void tulostaValikko()
{
	char v;
	struct Matkakortti kortti;  // kortti on struct Matkakortti -tyyppinen muuttuja
	nollaaKortti(&kortti); // huolehditaan ettei kortti n�yt� kamalalta, jos sit� ei alusteta

	do
	{
		system("cls");
		printf("---------------------------------Valikko------------------------------");
		printf("\n\n\n\n");
		printf("\t\t\tAlusta matkakortti\t\t\t1");
		printf("\n\t\t\tLataa saldoa\t\t\t\t2");
		printf("\n\t\t\tMatkusta Helsingiss� %.2fe\t\t3", HELSINKIHINTA);
		printf("\n\t\t\tMatkusta seutuliikenteess� %.2fe\t4", SEUTUHINTA);
		printf("\n\t\t\tTulosta kortin tiedot\t\t\t5");
		printf("\n\t\t\tLopetus\t\t\t\t\t6");
		printf("\n\t\t\tValitse:");


		v = _getch();
		switch (v)
		{
		case '1': 	
			alustaKortti(&kortti); // kortti v�litet��n aliohjelmalle muuttujaparametrina (k�ytt�en osoitinta)
			_getch();
			break;

		case '2':   
			lataaSaldo(&kortti);
			break;
		case '3':
			if (matkusta(&kortti, HELSINKI) == 0)
			{
				printf("\n\t\t\tHyv�� matkaa!");
				printf("\n\t\t\tSaldo: %.2f ", kortti.saldo);
			}
			else
			{
				printf("\n\t\t\tRahasi eiv�t riit�.");
				printf("\n\t\t\tSaldo: %.2f ", kortti.saldo);
			}
			_getch();
			break;
		case '4':	//if 
			if (matkusta(&kortti, SEUTU) == 0)
			{
				printf("\n\n\t\t\tHyv�� matkaa!");
				printf("\n\t\t\tSaldo: %.2f ", kortti.saldo);
			}
			else
			{
				printf("\n\n\t\t\tRahasi eiv�t riit�.");
				printf("\n\t\t\tSaldo: %.2f ", kortti.saldo);
			}
			_getch();
			break;
		case '5':
			printf("\n\n\n\t\t\tKortin tiedot:");
			printf("\n\t\t\tNimi: %s ", kortti.omistaja);
			printf("\n\t\t\tSaldo: %.2f ", kortti.saldo);
			_getch();
			break;

		case '6':

			break;

		}
	} while (v != '6');

}


void alustaKortti(struct Matkakortti* kortti)
{
	printf("\nAnna kortin omistajan nimi:");
	gets_s(kortti->omistaja, 41);
}

void nollaaKortti(struct Matkakortti* kortti)
{
	struct Matkakortti uusi = { "\0", 0 };
	*kortti = uusi;
}

void lataaSaldo(struct Matkakortti* kortti)
{
	printf("\nAnna ladattava summa: ");
	float summa;
	scanf_s("%f", &summa);
	kortti->saldo += summa;
}
// Teht�v�5: katsoo kortilta, onko saldo riitt�v� ja jos on, niin pienent�� sit�
int matkusta(struct Matkakortti* kortti, enum Matkatyyppi tyyppi)
{
	float hinta = 0;
	switch (tyyppi)
	{
		case HELSINKI: 
			hinta = HELSINKIHINTA;
			break;
	
		case SEUTU: 
			hinta = SEUTUHINTA;
			break;
	}

	float testi = kortti->saldo - hinta;

	if (testi >= 0)
	{
		kortti->saldo = testi;
		return 0;
	}
	else
	{
		return -1;
	}
}


