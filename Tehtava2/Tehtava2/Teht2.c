
#include <stdio.h>

// J�rjestyksen invertoija //
/*
void main()
{
	char buffer[21], tallennus[21];
	char *alku, *loppu;

	printf("Kirjoita jotain (20 merkki� max)");
	gets_s(buffer, 21);

	loppu = buffer;
	alku = tallennus;

	while (*loppu != '\0') //kelaa loppuun
	{
		loppu++;
	}

	do
	{
		loppu--;  //Aloita kelaamalla lopetusmerkki
		*alku = *loppu;
		alku++;
	} while (loppu != buffer);

	*alku = '\0'; //lis�� loppumerkki

	printf("%s", tallennus);
}
*/

// Merkin muuntaja pieni <-> ISO //
void main()
{
	char buffer[21], tallennus[21];
	char *kirjoitin, *lukija;
	int value;

	printf("Kirjoita jotain (20 merkki� max)");
	gets_s(buffer, 21);

	lukija = buffer;
	kirjoitin = tallennus;

	do
	{
		value = (int) *lukija;
		if (value < 91 && value > 64) 
		{
			*kirjoitin = (char)(value + 32);
		}
		else if (value < 123 && value > 96)
		{
			*kirjoitin = (char)(value - 32);
		}
		else
		{
			printf("bad character input, skipping");
		}

		lukija++;
		kirjoitin++;
	} while (*lukija != '\0');
	*kirjoitin = '\0';

	printf("%s", tallennus);
}