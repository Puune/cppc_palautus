#pragma once
#include "stdafx.h"

using namespace std;

class Matkakortti; // forward-määrittely 

class Leimaaja
{
private:
	string viimeisinLeimaaja = "none";
	struct tm viimeisinAikaleima;

public:
	bool leimaa(unique_ptr<Matkakortti>& kortti, Matkatyyppi tyyppi);
	void tulostaLeimaaja();
	void tulostaAikaleima(); // testilause
};

