// Matkajarjestelma.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int v;
	string rivi;
	float raha;
	unique_ptr<Matkakortti> kortti(new Matkakortti);
	Leimaaja leimaaja;
	
	do
	{
		system("cls");
		cout <<"-------------------Matkakortin testausvalikko--------------------";
		cout <<"\n\n\n\n";
		cout << "\t\t\t\tAlusta matkakortti 1.\n";
		cout << "\t\t\t\tLataa matkakortti 2.\n";
		cout << "\t\t\t\tMatkusta 3.\n";
		cout << "\t\t\t\tTulosta kortin tiedot 4.\n";
		cout << "\t\t\t\tTulosta leimaaja 5.\n";
		cout << "\t\t\t\tLopeta 6.\n";
		gotoxy(43,12);
		v=getIntFromStream();
		switch (v)
		{
			case 1:
				gotoxy(25, 14);
				cout << "Anna kortin omistajan nimi: ";
				getline(cin, rivi);
				(*kortti).alusta(rivi);
			break;
			case 2:
				gotoxy(30, 14);
				cout << "Anna lis�tt�v� saldo: ";
				raha = getFloatFromStream();
				(*kortti).lataa(raha);
			break;
			case 3:
				system("cls");
				cout << "Matkusta sis�isell� alueella (0)" << endl;
				cout << "Matkusta seutu alueella (1)" << endl;
				gotoxy(25, 14);
				if (leimaaja.leimaa(kortti, (Matkatyyppi(getIntFromStream()))))
				{
					cout << "Matkustit onnistuneesti" << endl;
				}
				else 
				{
					cout << "Saldosi ei riit�" << endl;
				}
				cin.get();
			break;
			case 4:       //
				cout << (*kortti).tulostaTiedot();
				cin.get();

			break;
			case 5:       //
				leimaaja.tulostaLeimaaja();

				cin.get();
			break;
			case 6:

				cin.get();
			break;				
		}
	}
	while (v!=6);

	return 0;
}



